<?php

namespace Drupal\helpdesk_zammad;

use Drupal\helpdesk_integration\Service as BaseService;
use Drupal\helpdesk_zammad\Plugin\HelpdeskIntegration\Zammad;

/**
 * Services for the Zammad helpdesk.
 */
class Service {

  /**
   * The helpdesk integration services.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected $service;

  /**
   * Service constructor.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk integration services.
   */
  public function __construct(BaseService $service) {
    $this->service = $service;
  }

  /**
   * Get a list of active Zammad instances.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface[]
   *   A list of active Zammad instances.
   */
  public function getZammadInstances(): array {
    $instances = [];
    foreach ($this->service->getHelpdeskInstances() as $helpdeskInstance) {
      $plugin = $helpdeskInstance->getPlugin();
      if ($plugin instanceof Zammad) {
        $instances[] = $helpdeskInstance;
      }
    }
    return $instances;
  }

  /**
   * Get a list of active Zammad instances.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface|bool
   *   The first active Zammad instance with chat support or FALSE.
   */
  public function getZammadChatInstance() {
    foreach ($this->getZammadInstances() as $zammadInstance) {
      if ($zammadInstance->get('chat_id')) {
        return $zammadInstance;
      }
    }
    return FALSE;
  }

}
